<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="grid grid-cols-5 gap-5">
        <aside class="md:col-span-2 bg-red-500">
            <section id="profile">
                <div class="w-full relative">
                    <img src="/images/me_ai.png" alt="" class="w-full h-96">
                    <p class="absolute bottom-5 left-5 text-xl">Amané Hosanna</p>
                </div>
                <div class="p-5">
                    <ul>
                        <li>
                            <i class="material-icons"></i>
                        </li>
                    </ul>
                </div>
            </section>
            <section id="skills">

            </section>
            <section id="languages">

            </section>
            <section id="hobbies">

            </section>
        </aside>
        <main class="md:col-span-3 bg-blue-500">
            <section id="work">

            </section>
            <section id="education">

            </section>
        </main>
    </div>
    <footer>

    </footer>
</body>
</html>
