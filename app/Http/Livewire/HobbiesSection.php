<?php

namespace App\Http\Livewire;

use Livewire\Component;

class HobbiesSection extends Component
{
    public $hobbies;

    function mount($hobbies){
        $this->hobbies = $hobbies;
    }
    public function render()
    {
        return view('livewire.hobbies-section');
    }
}
