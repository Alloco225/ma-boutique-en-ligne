<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProfileWorkAPIRequest;
use App\Http\Requests\API\UpdateProfileWorkAPIRequest;
use App\Models\ProfileWork;
use App\Repositories\ProfileWorkRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\ProfileWorkResource;
use Response;

/**
 * Class ProfileWorkController
 * @package App\Http\Controllers\API
 */

class ProfileWorkAPIController extends AppBaseController
{
    /** @var  ProfileWorkRepository */
    private $profileWorkRepository;

    public function __construct(ProfileWorkRepository $profileWorkRepo)
    {
        $this->profileWorkRepository = $profileWorkRepo;
    }

    /**
     * Display a listing of the ProfileWork.
     * GET|HEAD /profileWorks
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $profileWorks = $this->profileWorkRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(ProfileWorkResource::collection($profileWorks), 'Profile Works retrieved successfully');
    }

    /**
     * Store a newly created ProfileWork in storage.
     * POST /profileWorks
     *
     * @param CreateProfileWorkAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProfileWorkAPIRequest $request)
    {
        $input = $request->all();

        $profileWork = $this->profileWorkRepository->create($input);

        return $this->sendResponse(new ProfileWorkResource($profileWork), 'Profile Work saved successfully');
    }

    /**
     * Display the specified ProfileWork.
     * GET|HEAD /profileWorks/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ProfileWork $profileWork */
        $profileWork = $this->profileWorkRepository->find($id);

        if (empty($profileWork)) {
            return $this->sendError('Profile Work not found');
        }

        return $this->sendResponse(new ProfileWorkResource($profileWork), 'Profile Work retrieved successfully');
    }

    /**
     * Update the specified ProfileWork in storage.
     * PUT/PATCH /profileWorks/{id}
     *
     * @param int $id
     * @param UpdateProfileWorkAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfileWorkAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProfileWork $profileWork */
        $profileWork = $this->profileWorkRepository->find($id);

        if (empty($profileWork)) {
            return $this->sendError('Profile Work not found');
        }

        $profileWork = $this->profileWorkRepository->update($input, $id);

        return $this->sendResponse(new ProfileWorkResource($profileWork), 'ProfileWork updated successfully');
    }

    /**
     * Remove the specified ProfileWork from storage.
     * DELETE /profileWorks/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ProfileWork $profileWork */
        $profileWork = $this->profileWorkRepository->find($id);

        if (empty($profileWork)) {
            return $this->sendError('Profile Work not found');
        }

        $profileWork->delete();

        return $this->sendSuccess('Profile Work deleted successfully');
    }
}
