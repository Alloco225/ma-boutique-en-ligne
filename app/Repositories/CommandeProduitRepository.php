<?php

namespace App\Repositories;

use App\Models\CommandeProduit;
use App\Repositories\BaseRepository;

/**
 * Class CommandeProduitRepository
 * @package App\Repositories
 * @version October 11, 2021, 4:23 pm UTC
*/

class CommandeProduitRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_commande',
        'id_produit',
        'quantite',
        'montant'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommandeProduit::class;
    }
}
