<?php

namespace App\Repositories;

use App\Models\Commande;
use App\Repositories\BaseRepository;

/**
 * Class CommandeRepository
 * @package App\Repositories
 * @version October 11, 2021, 4:22 pm UTC
*/

class CommandeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'total',
        'id_utilisateur'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Commande::class;
    }
}
