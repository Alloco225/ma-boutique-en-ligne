<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateQuoteRequestAPIRequest;
use App\Http\Requests\API\UpdateQuoteRequestAPIRequest;
use App\Models\QuoteRequest;
use App\Repositories\QuoteRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\QuoteRequestResource;
use Response;

/**
 * Class QuoteRequestController
 * @package App\Http\Controllers\API
 */

class QuoteRequestAPIController extends AppBaseController
{
    /** @var  QuoteRequestRepository */
    private $quoteRequestRepository;

    public function __construct(QuoteRequestRepository $quoteRequestRepo)
    {
        $this->quoteRequestRepository = $quoteRequestRepo;
    }

    /**
     * Display a listing of the QuoteRequest.
     * GET|HEAD /quoteRequests
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $quoteRequests = $this->quoteRequestRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(QuoteRequestResource::collection($quoteRequests), 'Quote Requests retrieved successfully');
    }

    /**
     * Store a newly created QuoteRequest in storage.
     * POST /quoteRequests
     *
     * @param CreateQuoteRequestAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateQuoteRequestAPIRequest $request)
    {
        $input = $request->all();

        $quoteRequest = $this->quoteRequestRepository->create($input);

        return $this->sendResponse(new QuoteRequestResource($quoteRequest), 'Quote Request saved successfully');
    }

    /**
     * Display the specified QuoteRequest.
     * GET|HEAD /quoteRequests/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var QuoteRequest $quoteRequest */
        $quoteRequest = $this->quoteRequestRepository->find($id);

        if (empty($quoteRequest)) {
            return $this->sendError('Quote Request not found');
        }

        return $this->sendResponse(new QuoteRequestResource($quoteRequest), 'Quote Request retrieved successfully');
    }

    /**
     * Update the specified QuoteRequest in storage.
     * PUT/PATCH /quoteRequests/{id}
     *
     * @param int $id
     * @param UpdateQuoteRequestAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQuoteRequestAPIRequest $request)
    {
        $input = $request->all();

        /** @var QuoteRequest $quoteRequest */
        $quoteRequest = $this->quoteRequestRepository->find($id);

        if (empty($quoteRequest)) {
            return $this->sendError('Quote Request not found');
        }

        $quoteRequest = $this->quoteRequestRepository->update($input, $id);

        return $this->sendResponse(new QuoteRequestResource($quoteRequest), 'QuoteRequest updated successfully');
    }

    /**
     * Remove the specified QuoteRequest from storage.
     * DELETE /quoteRequests/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var QuoteRequest $quoteRequest */
        $quoteRequest = $this->quoteRequestRepository->find($id);

        if (empty($quoteRequest)) {
            return $this->sendError('Quote Request not found');
        }

        $quoteRequest->delete();

        return $this->sendSuccess('Quote Request deleted successfully');
    }
}
