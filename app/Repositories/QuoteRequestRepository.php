<?php

namespace App\Repositories;

use App\Models\QuoteRequest;
use App\Repositories\BaseRepository;

/**
 * Class QuoteRequestRepository
 * @package App\Repositories
 * @version July 27, 2021, 1:43 am UTC
*/

class QuoteRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'full_name',
        'email',
        'company_name',
        'company_description',
        'company_link',
        'reason_for_rebuild',
        'competitor_links',
        'logo_url',
        'app_size',
        'ui_level',
        'user_account_email',
        'user_account_phone',
        'user_account_facebook',
        'user_account_twitter',
        'user_account_google',
        'user_account_linkedin',
        'user_account_github',
        'user_account_email_invitation',
        'user_account_multitenant',
        'user_account_subdomain',
        'user_account_custom_domain',
        'ugc_dashboard',
        'ugc_activity_feed',
        'ugc_file_upload',
        'ugc_user_profile',
        'ugc_transactional_emails',
        'ugc_tags',
        'ugc_ratings_or_ratings',
        'ugc_media_processing',
        'ugc_free_text_searching',
        'date_calendaring',
        'map_geolocation',
        'custom_map',
        'bookings',
        'in_app_messaging',
        'forums_and_commenting',
        'social_sharing',
        'subscription_plans',
        'payment_processing',
        'shoping_cart',
        'user_marketplace',
        'product_management',
        'cms',
        'user_admin_pages',
        'content_moderation',
        'intercom_or_chatbot',
        'usage_analytics',
        'crash_reporting',
        'performance_monitoring',
        'multilingual_support',
        'third_party_apis',
        'api',
        'sms_messaging',
        'blog',
        'user_testimonials',
        'ssl_certificate',
        'dos_protection',
        'two_factor_auth',
        'app_icon_design',
        'cloud_syncing',
        'barcode_qrcode',
        'notifications'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return QuoteRequest::class;
    }
}
