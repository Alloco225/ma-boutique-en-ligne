<div class="table-responsive">
    <table class="table" id="commandes-table">
        <thead>
            <tr>
                <th>Total</th>
        <th>Id Utilisateur</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($commandes as $commande)
            <tr>
                <td>{{ $commande->total }}</td>
            <td>{{ $commande->id_utilisateur }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['commandes.destroy', $commande->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('commandes.show', [$commande->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('commandes.edit', [$commande->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
