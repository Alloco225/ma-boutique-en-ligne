<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProfileTechnologyGroupAPIRequest;
use App\Http\Requests\API\UpdateProfileTechnologyGroupAPIRequest;
use App\Models\ProfileTechnologyGroup;
use App\Repositories\ProfileTechnologyGroupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\ProfileTechnologyGroupResource;
use Response;

/**
 * Class ProfileTechnologyGroupController
 * @package App\Http\Controllers\API
 */

class ProfileTechnologyGroupAPIController extends AppBaseController
{
    /** @var  ProfileTechnologyGroupRepository */
    private $profileTechnologyGroupRepository;

    public function __construct(ProfileTechnologyGroupRepository $profileTechnologyGroupRepo)
    {
        $this->profileTechnologyGroupRepository = $profileTechnologyGroupRepo;
    }

    /**
     * Display a listing of the ProfileTechnologyGroup.
     * GET|HEAD /profileTechnologyGroups
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $profileTechnologyGroups = $this->profileTechnologyGroupRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(ProfileTechnologyGroupResource::collection($profileTechnologyGroups), 'Profile Technology Groups retrieved successfully');
    }

    /**
     * Store a newly created ProfileTechnologyGroup in storage.
     * POST /profileTechnologyGroups
     *
     * @param CreateProfileTechnologyGroupAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProfileTechnologyGroupAPIRequest $request)
    {
        $input = $request->all();

        $profileTechnologyGroup = $this->profileTechnologyGroupRepository->create($input);

        return $this->sendResponse(new ProfileTechnologyGroupResource($profileTechnologyGroup), 'Profile Technology Group saved successfully');
    }

    /**
     * Display the specified ProfileTechnologyGroup.
     * GET|HEAD /profileTechnologyGroups/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ProfileTechnologyGroup $profileTechnologyGroup */
        $profileTechnologyGroup = $this->profileTechnologyGroupRepository->find($id);

        if (empty($profileTechnologyGroup)) {
            return $this->sendError('Profile Technology Group not found');
        }

        return $this->sendResponse(new ProfileTechnologyGroupResource($profileTechnologyGroup), 'Profile Technology Group retrieved successfully');
    }

    /**
     * Update the specified ProfileTechnologyGroup in storage.
     * PUT/PATCH /profileTechnologyGroups/{id}
     *
     * @param int $id
     * @param UpdateProfileTechnologyGroupAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfileTechnologyGroupAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProfileTechnologyGroup $profileTechnologyGroup */
        $profileTechnologyGroup = $this->profileTechnologyGroupRepository->find($id);

        if (empty($profileTechnologyGroup)) {
            return $this->sendError('Profile Technology Group not found');
        }

        $profileTechnologyGroup = $this->profileTechnologyGroupRepository->update($input, $id);

        return $this->sendResponse(new ProfileTechnologyGroupResource($profileTechnologyGroup), 'ProfileTechnologyGroup updated successfully');
    }

    /**
     * Remove the specified ProfileTechnologyGroup from storage.
     * DELETE /profileTechnologyGroups/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ProfileTechnologyGroup $profileTechnologyGroup */
        $profileTechnologyGroup = $this->profileTechnologyGroupRepository->find($id);

        if (empty($profileTechnologyGroup)) {
            return $this->sendError('Profile Technology Group not found');
        }

        $profileTechnologyGroup->delete();

        return $this->sendSuccess('Profile Technology Group deleted successfully');
    }
}
