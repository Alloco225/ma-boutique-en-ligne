<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfileHobbyRequest;
use App\Http\Requests\UpdateProfileHobbyRequest;
use App\Repositories\ProfileHobbyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProfileHobbyController extends AppBaseController
{
    /** @var  ProfileHobbyRepository */
    private $profileHobbyRepository;

    public function __construct(ProfileHobbyRepository $profileHobbyRepo)
    {
        $this->profileHobbyRepository = $profileHobbyRepo;
    }

    /**
     * Display a listing of the ProfileHobby.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $profileHobbies = $this->profileHobbyRepository->all();

        return view('profile_hobbies.index')
            ->with('profileHobbies', $profileHobbies);
    }

    /**
     * Show the form for creating a new ProfileHobby.
     *
     * @return Response
     */
    public function create()
    {
        return view('profile_hobbies.create');
    }

    /**
     * Store a newly created ProfileHobby in storage.
     *
     * @param CreateProfileHobbyRequest $request
     *
     * @return Response
     */
    public function store(CreateProfileHobbyRequest $request)
    {
        $input = $request->all();

        $profileHobby = $this->profileHobbyRepository->create($input);

        Flash::success('Profile Hobby saved successfully.');

        return redirect(route('profileHobbies.index'));
    }

    /**
     * Display the specified ProfileHobby.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $profileHobby = $this->profileHobbyRepository->find($id);

        if (empty($profileHobby)) {
            Flash::error('Profile Hobby not found');

            return redirect(route('profileHobbies.index'));
        }

        return view('profile_hobbies.show')->with('profileHobby', $profileHobby);
    }

    /**
     * Show the form for editing the specified ProfileHobby.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $profileHobby = $this->profileHobbyRepository->find($id);

        if (empty($profileHobby)) {
            Flash::error('Profile Hobby not found');

            return redirect(route('profileHobbies.index'));
        }

        return view('profile_hobbies.edit')->with('profileHobby', $profileHobby);
    }

    /**
     * Update the specified ProfileHobby in storage.
     *
     * @param int $id
     * @param UpdateProfileHobbyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfileHobbyRequest $request)
    {
        $profileHobby = $this->profileHobbyRepository->find($id);

        if (empty($profileHobby)) {
            Flash::error('Profile Hobby not found');

            return redirect(route('profileHobbies.index'));
        }

        $profileHobby = $this->profileHobbyRepository->update($request->all(), $id);

        Flash::success('Profile Hobby updated successfully.');

        return redirect(route('profileHobbies.index'));
    }

    /**
     * Remove the specified ProfileHobby from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $profileHobby = $this->profileHobbyRepository->find($id);

        if (empty($profileHobby)) {
            Flash::error('Profile Hobby not found');

            return redirect(route('profileHobbies.index'));
        }

        $this->profileHobbyRepository->delete($id);

        Flash::success('Profile Hobby deleted successfully.');

        return redirect(route('profileHobbies.index'));
    }
}
