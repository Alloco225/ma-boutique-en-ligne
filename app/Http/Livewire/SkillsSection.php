<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SkillsSection extends Component
{
    public $skills;

    function mount($skills){
        $this->skills = $skills;
    }

    public function render()
    {
        return view('livewire.skills-section');
    }
}
