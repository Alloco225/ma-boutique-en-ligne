@extends('layouts.app')


@section('content')
    <main class="p-5 bg-blue-50">
        <nav>
            Produits Ajoutés : {{$produits_ajoutes}}
        </nav>
        <div class="flex flex-wrap gap-5">
            @foreach($produits as $produit)
                <div class="rounded bg-white p-5 ">
                    <h2 class="text-xl font-bold">
                        Nom: {{$produit->name}}
                    </h2>
                    <h4 class="text-sm font-semibold">
                        prix: {{$produit->price}}
                    </h4>
                    <a href="/ajouter/{{$produit->id}}" class="block bg-red-500 mt-5 px-3 py-1 text-white rounded">Commander</a>
                </div>
            @endforeach
        </div>
    </main>
@endsection
