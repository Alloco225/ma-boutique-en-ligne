@extends('layouts.app')

@section('content')
    <main>
        <nav></nav>
        <section id="showcase" class="py-10 ">
            <div class="width md:grid md:grid-cols-2 gap-10">
                <div class="md:hidden mb-10">
                    <img src="{{env('APP_ICON_URL')}}" class="img-fluid" alt="">
                </div>
                <div class="flex flex-col justify-center ">
                    <h1 class="text-2xl md:text-2xl font-bold mb-5">Vendez, Achetez, Bradez</h1>
                    <div class="sm:w-full">
                        <div class="flex justify-center sm:justify-start mb-3">
                            <input type="text" class="border-none bg-gray-100 rounded rounded-r-none">
                            <button class="px-2 py-1 bg-green-400 border rounded rounded-l-none">Chercher</button>
                        </div>
                        <div class="flex items-center">
                            <span>Ou plutôt</span>
                            <a href="" class="rounded border border-green-400 px-2 py-1 ml-3">Vendre</a>
                        </div>
                    </div>
                </div>
                {{-- Image --}}
                <div class="hidden md:block">
                    <img src="{{env('APP_ICON_URL')}}" class="img-fluid" alt="">
                </div>
            </div>
        </section>
        <section id="how-it-works" class="py-20 bg-gray-100">
            <div class="width">
                <div class="text-center mb-16 md:mb-10">
                    <h2 class="text-2xl font-semibold">
                        Comment ça marche
                    </h2>
                    <p>Lorem ipsum dolor sit amet.</p>
                </div>
                <div class="sm:grid grid-cols-2 sm:grid-cols-3 gap-y-10  gap-5 md:gap-10 text-center">
                    <div class="mb-10 text-left sm:text-center md:mb-5">
                        <img src="{{env('APP_ICON_URL')}}" class="mr-auto sm:mx-auto w-32 h-auto" alt="">
                        <div class="mt-2 md:mt-5">Créez un compte</div>
                    </div>

                    <div class="mb-10 text-right sm:text-center md:mb-5">
                        <img src="{{env('APP_ICON_URL')}}" class="ml-auto sm:mx-auto w-32 h-auto" alt="">
                        <div class="mt-2 md:mt-5">Postez votre produit</div>
                    </div>
                    <div class="mb-10 text-left sm:text-center md:mb-5">
                        <img src="{{env('APP_ICON_URL')}}" class="mr-auto sm:mx-auto w-32 h-auto" alt="">
                        <div class="mt-2 md:mt-5">Gagnez de l'argent</div>
                    </div>
                </div>
                <div class="flex justify-center mt-10">
                    <a href="" class="px-2 py-2 md:py-1 mt-5 md:mt-0 w-full md:w-auto text-center rounded bg-green-400">Créer un compte</a>
                </div>
            </div>
        </section>
        <section id="great-for" class="py-20">
            <div class="width md:grid grid-cols-2 gap-10">
                <div class="flex flex-col justify-center">
                    <h1 class="text-2xl font-semibold mb-5">Pour tout le monde</h1>
                    <p class="mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, iste.</p>
                    <p>Lorem ipsum dolor sit amet consectetur.</p>
                </div>
                <div class="bg-gray-100 rounded-md mt-10 md:mt-0 py-10">
                    <img src="{{env('APP_ICON_URL')}}" class="w-52 h-auto mx-auto" alt="">
                </div>
            </div>
            {{-- Businnesses --}}
            <div class="width mt-16 text-center grid grid-cols-2 md:grid-cols-3 gap-5 md:gap-y-10">
                @for ($i = 0; $i < 5; $i++)
                    <div class="rounded-lg hover:bg-gray-100 cursor-pointer py-2 md:py-10">
                        <span class="rounded p-2 mx-auto bg-gray-200 inline-block">
                            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19.428 15.428a2 2 0 00-1.022-.547l-2.387-.477a6 6 0 00-3.86.517l-.318.158a6 6 0 01-3.86.517L6.05 15.21a2 2 0 00-1.806.547M8 4h8l-1 1v5.172a2 2 0 00.586 1.414l5 5c1.26 1.26.367 3.414-1.415 3.414H4.828c-1.782 0-2.674-2.154-1.414-3.414l5-5A2 2 0 009 10.172V5L8 4z"></path></svg>
                        </span>
                        <h4 class="mt-2 md:mt-5 text-md md:text-lg font-semibold">Business name</h4>
                        <p class="text-sm text-gray-400">Lorem ipsum dolor, sit amet.</p>
                    </div>
                @endfor
                <div class="flex flex-col justify-center rounded-lg hover:bg-gray-100 cursor-pointer">
                    <span class="rounded p-2 mx-auto bg-gray-200 inline-block">
                        <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 14v6m-3-3h6M6 10h2a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2zm10 0h2a2 2 0 002-2V6a2 2 0 00-2-2h-2a2 2 0 00-2 2v2a2 2 0 002 2zM6 20h2a2 2 0 002-2v-2a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2z"></path></svg>
                    </span>
                    <a href="" class="mt-4">+ Ajouter mon business</a>
                </div>
            </div>
        </section>
        <section id="debarassez-vous" class="py-20 bg-green-400 text-white">
            <div class="width md:grid grid-cols-2 gap-10">
                <div class="flex flex-col justify-center">
                    <h2 class="mb-10 text-2xl font-semibold">La meilleure façon de brader</h2>
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ullam fuga accusantium iusto!</p>

                    <div class="mt-5 flex items-center">
                        <span class="inline-flex justify-center items-center p-2 rounded bg-gray-700">
                            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M14.243 5.757a6 6 0 10-.986 9.284 1 1 0 111.087 1.678A8 8 0 1118 10a3 3 0 01-4.8 2.401A4 4 0 1114 10a1 1 0 102 0c0-1.537-.586-3.07-1.757-4.243zM12 10a2 2 0 10-4 0 2 2 0 004 0z" clip-rule="evenodd"></path></svg>
                        </span>
                        <span class="ml-5">
                            Vos produits sont bradé sur facebook
                        </span>
                    </div>
                    <div class="mt-3 flex items-center">
                        <span class="inline-flex justify-center items-center p-2 rounded bg-gray-700">
                            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M14.243 5.757a6 6 0 10-.986 9.284 1 1 0 111.087 1.678A8 8 0 1118 10a3 3 0 01-4.8 2.401A4 4 0 1114 10a1 1 0 102 0c0-1.537-.586-3.07-1.757-4.243zM12 10a2 2 0 10-4 0 2 2 0 004 0z" clip-rule="evenodd"></path></svg>
                        </span>
                        <span class="ml-5">
                            Partagez vos produits sur twitter
                        </span>
                    </div>

                    <div class="mt-5 flex">
                        <a href="" class="rounded-md bg-green-500 w-full px-3 py-2 text-center">Commencer à gagner de l'argent</a>
                    </div>
                </div>
                <div class="hidden md:block">
                    <img src="{{env('APP_ICON_URL')}}" class="mx-auto w-64 mt-auto" alt="">
                </div>
            </div>
        </section>
        <section id="how-much-money" class="py-20">
            <div class="width md:grid grid-cols-2 gap-10">
                <div class="rounded-md py-5 md bg-gray-100">
                    <img src="{{env('APP_ICON_URL')}}" class="mx-auto" alt="">
                </div>
                <div class="flex mt-10 md:mt-0 flex-col justify-center">
                    <h4 class="text-2xl font-semibold mb-10">Comment se faire de l'argent ?</h4>
                    <div class="text-gray-400">
                        <p class="mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni maxime dolore consequuntur sequi, itaque reprehenderit.</p>

                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo, vel!</p>
                    </div>
                    <div class="mt-10 flex md:justify-end">
                        <a href="" class="bg-green-400 w-full md:w-auto text-center px-3 py-2 rounded">Commencer maintenant</a>
                    </div>
                </div>
            </div>
        </section>
        <footer class="">
            <div class="bg-gray-400 py-10">
                <div class="width text-center">
                    Cocovico.com
                </div>
            </div>
            <div class="bg-gray-700 text-center py-4 text-white">
                Copyright &copy; 2021
            </div>
        </footer>
    </main>
@endsection
