<!-- Id Commande Field -->
<div class="col-sm-12">
    {!! Form::label('id_commande', 'Id Commande:') !!}
    <p>{{ $commandeProduit->id_commande }}</p>
</div>

<!-- Id Produit Field -->
<div class="col-sm-12">
    {!! Form::label('id_produit', 'Id Produit:') !!}
    <p>{{ $commandeProduit->id_produit }}</p>
</div>

<!-- Quantite Field -->
<div class="col-sm-12">
    {!! Form::label('quantite', 'Quantite:') !!}
    <p>{{ $commandeProduit->quantite }}</p>
</div>

<!-- Montant Field -->
<div class="col-sm-12">
    {!! Form::label('montant', 'Montant:') !!}
    <p>{{ $commandeProduit->montant }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $commandeProduit->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $commandeProduit->updated_at }}</p>
</div>

