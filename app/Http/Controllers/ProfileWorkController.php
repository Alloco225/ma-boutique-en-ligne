<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfileWorkRequest;
use App\Http\Requests\UpdateProfileWorkRequest;
use App\Repositories\ProfileWorkRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProfileWorkController extends AppBaseController
{
    /** @var  ProfileWorkRepository */
    private $profileWorkRepository;

    public function __construct(ProfileWorkRepository $profileWorkRepo)
    {
        $this->profileWorkRepository = $profileWorkRepo;
    }

    /**
     * Display a listing of the ProfileWork.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $profileWorks = $this->profileWorkRepository->all();

        return view('profile_works.index')
            ->with('profileWorks', $profileWorks);
    }

    /**
     * Show the form for creating a new ProfileWork.
     *
     * @return Response
     */
    public function create()
    {
        return view('profile_works.create');
    }

    /**
     * Store a newly created ProfileWork in storage.
     *
     * @param CreateProfileWorkRequest $request
     *
     * @return Response
     */
    public function store(CreateProfileWorkRequest $request)
    {
        $input = $request->all();

        $profileWork = $this->profileWorkRepository->create($input);

        Flash::success('Profile Work saved successfully.');

        return redirect(route('profileWorks.index'));
    }

    /**
     * Display the specified ProfileWork.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $profileWork = $this->profileWorkRepository->find($id);

        if (empty($profileWork)) {
            Flash::error('Profile Work not found');

            return redirect(route('profileWorks.index'));
        }

        return view('profile_works.show')->with('profileWork', $profileWork);
    }

    /**
     * Show the form for editing the specified ProfileWork.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $profileWork = $this->profileWorkRepository->find($id);

        if (empty($profileWork)) {
            Flash::error('Profile Work not found');

            return redirect(route('profileWorks.index'));
        }

        return view('profile_works.edit')->with('profileWork', $profileWork);
    }

    /**
     * Update the specified ProfileWork in storage.
     *
     * @param int $id
     * @param UpdateProfileWorkRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfileWorkRequest $request)
    {
        $profileWork = $this->profileWorkRepository->find($id);

        if (empty($profileWork)) {
            Flash::error('Profile Work not found');

            return redirect(route('profileWorks.index'));
        }

        $profileWork = $this->profileWorkRepository->update($request->all(), $id);

        Flash::success('Profile Work updated successfully.');

        return redirect(route('profileWorks.index'));
    }

    /**
     * Remove the specified ProfileWork from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $profileWork = $this->profileWorkRepository->find($id);

        if (empty($profileWork)) {
            Flash::error('Profile Work not found');

            return redirect(route('profileWorks.index'));
        }

        $this->profileWorkRepository->delete($id);

        Flash::success('Profile Work deleted successfully.');

        return redirect(route('profileWorks.index'));
    }
}
