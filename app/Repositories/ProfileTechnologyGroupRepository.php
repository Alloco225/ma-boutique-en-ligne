<?php

namespace App\Repositories;

use App\Models\ProfileTechnologyGroup;
use App\Repositories\BaseRepository;

/**
 * Class ProfileTechnologyGroupRepository
 * @package App\Repositories
 * @version February 13, 2021, 11:36 am UTC
*/

class ProfileTechnologyGroupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'profile_id',
        'html_icon',
        'label',
        'value'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProfileTechnologyGroup::class;
    }
}
