<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ClientQuoteRequestForm extends Component
{
    public function render()
    {
        return view('livewire.client-quote-request-form');
    }
}
