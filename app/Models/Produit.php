<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as Model;



/**
 * Class Produit
 * @package App\Models
 * @version October 11, 2021, 4:21 pm UTC
 *
 * @property string $name
 * @property string $description
 * @property string $price
 * @property integer $stock
 */
class Produit extends Model
{

    use HasFactory;
    public $table = 'produits';




    public $fillable = [
        'name',
        'description',
        'price',
        'stock'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'price' => 'string',
        'stock' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    function commandes_produit(){
        return $this->hasMany(CommandeProduit::class);
    }
}
