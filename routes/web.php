<?php

use App\Models\Produit;
use App\Models\Commande;
use Illuminate\Http\Response;
use App\Http\Livewire\MyResume;
use App\Models\CommandeProduit;
use App\Http\Livewire\WelcomePage;
use App\Http\Livewire\WelcomeFancy;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\ClientQuoteRequestPage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    // Get products
    $produits = Produit::all();
    $produits_ajoutes = CommandeProduit::count();

    return view('produits', compact('produits', 'produits_ajoutes'));
});

Route::get('/ajouter/{id_produit}', function($id_produit){
    // Get products

    $produit = Produit::find($id_produit);

    if(!$produit){
        return redirect()->back();
    }

    $commande = Commande::create([
        'id_utilisateur' => 1,
        'total' => 0,
    ]);

    $produit_commande = CommandeProduit::create([
        'id_produit' => $produit->id,
        'id_commande' => $commande->id,
        'quantite' => 1,
        'montant' => $produit->price * 1,
    ]);

    $produits_ajoutes = CommandeProduit::count();

    $produits = Produit::all();
    return redirect()->back();
});


Route::any('{r}', function(){
    return view('placeholder');
})->where('r', '.*');


Route::resource('produits', App\Http\Controllers\ProduitController::class);

Route::resource('commandes', App\Http\Controllers\CommandeController::class);

Route::resource('commandeProduits', App\Http\Controllers\CommandeProduitController::class);
