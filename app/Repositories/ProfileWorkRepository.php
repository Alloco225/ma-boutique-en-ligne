<?php

namespace App\Repositories;

use App\Models\ProfileWork;
use App\Repositories\BaseRepository;

/**
 * Class ProfileWorkRepository
 * @package App\Repositories
 * @version February 13, 2021, 12:04 pm UTC
*/

class ProfileWorkRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'profile_id',
        'label',
        'thumbnail',
        'logo',
        'image',
        'cover',
        'description',
        'url'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProfileWork::class;
    }
}
