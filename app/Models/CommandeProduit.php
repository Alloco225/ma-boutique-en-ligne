<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;



/**
 * Class CommandeProduit
 * @package App\Models
 * @version October 11, 2021, 4:23 pm UTC
 *
 * @property unsignedBigInteger $id_commande
 * @property unsignedBigInteger $id_produit
 * @property integer $quantite
 * @property number $montant
 */
class CommandeProduit extends Model
{


    public $table = 'commande_produits';




    public $fillable = [
        'id_commande',
        'id_produit',
        'quantite',
        'montant'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'quantite' => 'integer',
        'montant' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

     // relationships
    function produit(){
        return $this->belongsTo(Produit::class);
    }
    function commande(){
        return $this->belongsTo(Commande::class);
    }
}
