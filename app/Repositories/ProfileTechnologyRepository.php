<?php

namespace App\Repositories;

use App\Models\ProfileTechnology;
use App\Repositories\BaseRepository;

/**
 * Class ProfileTechnologyRepository
 * @package App\Repositories
 * @version February 13, 2021, 1:18 am UTC
*/

class ProfileTechnologyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'html_image',
        'image_url',
        'label',
        'progress'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProfileTechnology::class;
    }
}
