<?php

namespace App\Http\Livewire;

use App\Models\Profile;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class LanguagesSection extends Component
{
    public $languages;
    public $secondary_languages;

    function mount($languages){
        $this->languages = $languages;
        // $this->secondary_languages = $languages->except($this->languages);
        $this->secondary_languages = $this->languages->splice(3);
    }
    public function render()
    {
        return view('livewire.languages-section');
    }
}
