<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommandeProduitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'id_commande' => $this->id_commande,
            'id_produit' => $this->id_produit,
            'quantite' => $this->quantite,
            'montant' => $this->montant,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
