<!-- Total Field -->
<div class="col-sm-12">
    {!! Form::label('total', 'Total:') !!}
    <p>{{ $commande->total }}</p>
</div>

<!-- Id Utilisateur Field -->
<div class="col-sm-12">
    {!! Form::label('id_utilisateur', 'Id Utilisateur:') !!}
    <p>{{ $commande->id_utilisateur }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $commande->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $commande->updated_at }}</p>
</div>

