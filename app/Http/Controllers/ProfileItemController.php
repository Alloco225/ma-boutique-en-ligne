<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfileItemRequest;
use App\Http\Requests\UpdateProfileItemRequest;
use App\Repositories\ProfileItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProfileItemController extends AppBaseController
{
    /** @var  ProfileItemRepository */
    private $profileItemRepository;

    public function __construct(ProfileItemRepository $profileItemRepo)
    {
        $this->profileItemRepository = $profileItemRepo;
    }

    /**
     * Display a listing of the ProfileItem.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $profileItems = $this->profileItemRepository->all();

        return view('profile_items.index')
            ->with('profileItems', $profileItems);
    }

    /**
     * Show the form for creating a new ProfileItem.
     *
     * @return Response
     */
    public function create()
    {
        return view('profile_items.create');
    }

    /**
     * Store a newly created ProfileItem in storage.
     *
     * @param CreateProfileItemRequest $request
     *
     * @return Response
     */
    public function store(CreateProfileItemRequest $request)
    {
        $input = $request->all();

        $profileItem = $this->profileItemRepository->create($input);

        Flash::success('Profile Item saved successfully.');

        return redirect(route('profileItems.index'));
    }

    /**
     * Display the specified ProfileItem.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $profileItem = $this->profileItemRepository->find($id);

        if (empty($profileItem)) {
            Flash::error('Profile Item not found');

            return redirect(route('profileItems.index'));
        }

        return view('profile_items.show')->with('profileItem', $profileItem);
    }

    /**
     * Show the form for editing the specified ProfileItem.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $profileItem = $this->profileItemRepository->find($id);

        if (empty($profileItem)) {
            Flash::error('Profile Item not found');

            return redirect(route('profileItems.index'));
        }

        return view('profile_items.edit')->with('profileItem', $profileItem);
    }

    /**
     * Update the specified ProfileItem in storage.
     *
     * @param int $id
     * @param UpdateProfileItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfileItemRequest $request)
    {
        $profileItem = $this->profileItemRepository->find($id);

        if (empty($profileItem)) {
            Flash::error('Profile Item not found');

            return redirect(route('profileItems.index'));
        }

        $profileItem = $this->profileItemRepository->update($request->all(), $id);

        Flash::success('Profile Item updated successfully.');

        return redirect(route('profileItems.index'));
    }

    /**
     * Remove the specified ProfileItem from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $profileItem = $this->profileItemRepository->find($id);

        if (empty($profileItem)) {
            Flash::error('Profile Item not found');

            return redirect(route('profileItems.index'));
        }

        $this->profileItemRepository->delete($id);

        Flash::success('Profile Item deleted successfully.');

        return redirect(route('profileItems.index'));
    }
}
