<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfileSkillRequest;
use App\Http\Requests\UpdateProfileSkillRequest;
use App\Repositories\ProfileSkillRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProfileSkillController extends AppBaseController
{
    /** @var  ProfileSkillRepository */
    private $profileSkillRepository;

    public function __construct(ProfileSkillRepository $profileSkillRepo)
    {
        $this->profileSkillRepository = $profileSkillRepo;
    }

    /**
     * Display a listing of the ProfileSkill.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $profileSkills = $this->profileSkillRepository->all();

        return view('profile_skills.index')
            ->with('profileSkills', $profileSkills);
    }

    /**
     * Show the form for creating a new ProfileSkill.
     *
     * @return Response
     */
    public function create()
    {
        return view('profile_skills.create');
    }

    /**
     * Store a newly created ProfileSkill in storage.
     *
     * @param CreateProfileSkillRequest $request
     *
     * @return Response
     */
    public function store(CreateProfileSkillRequest $request)
    {
        $input = $request->all();

        $profileSkill = $this->profileSkillRepository->create($input);

        Flash::success('Profile Skill saved successfully.');

        return redirect(route('profileSkills.index'));
    }

    /**
     * Display the specified ProfileSkill.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $profileSkill = $this->profileSkillRepository->find($id);

        if (empty($profileSkill)) {
            Flash::error('Profile Skill not found');

            return redirect(route('profileSkills.index'));
        }

        return view('profile_skills.show')->with('profileSkill', $profileSkill);
    }

    /**
     * Show the form for editing the specified ProfileSkill.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $profileSkill = $this->profileSkillRepository->find($id);

        if (empty($profileSkill)) {
            Flash::error('Profile Skill not found');

            return redirect(route('profileSkills.index'));
        }

        return view('profile_skills.edit')->with('profileSkill', $profileSkill);
    }

    /**
     * Update the specified ProfileSkill in storage.
     *
     * @param int $id
     * @param UpdateProfileSkillRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfileSkillRequest $request)
    {
        $profileSkill = $this->profileSkillRepository->find($id);

        if (empty($profileSkill)) {
            Flash::error('Profile Skill not found');

            return redirect(route('profileSkills.index'));
        }

        $profileSkill = $this->profileSkillRepository->update($request->all(), $id);

        Flash::success('Profile Skill updated successfully.');

        return redirect(route('profileSkills.index'));
    }

    /**
     * Remove the specified ProfileSkill from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $profileSkill = $this->profileSkillRepository->find($id);

        if (empty($profileSkill)) {
            Flash::error('Profile Skill not found');

            return redirect(route('profileSkills.index'));
        }

        $this->profileSkillRepository->delete($id);

        Flash::success('Profile Skill deleted successfully.');

        return redirect(route('profileSkills.index'));
    }
}
