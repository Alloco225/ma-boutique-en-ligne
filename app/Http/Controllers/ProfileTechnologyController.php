<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfileTechnologyRequest;
use App\Http\Requests\UpdateProfileTechnologyRequest;
use App\Repositories\ProfileTechnologyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProfileTechnologyController extends AppBaseController
{
    /** @var  ProfileTechnologyRepository */
    private $profileTechnologyRepository;

    public function __construct(ProfileTechnologyRepository $profileTechnologyRepo)
    {
        $this->profileTechnologyRepository = $profileTechnologyRepo;
    }

    /**
     * Display a listing of the ProfileTechnology.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $profileTechnologies = $this->profileTechnologyRepository->all();

        return view('profile_technologies.index')
            ->with('profileTechnologies', $profileTechnologies);
    }

    /**
     * Show the form for creating a new ProfileTechnology.
     *
     * @return Response
     */
    public function create()
    {
        return view('profile_technologies.create');
    }

    /**
     * Store a newly created ProfileTechnology in storage.
     *
     * @param CreateProfileTechnologyRequest $request
     *
     * @return Response
     */
    public function store(CreateProfileTechnologyRequest $request)
    {
        $input = $request->all();

        $profileTechnology = $this->profileTechnologyRepository->create($input);

        Flash::success('Profile Technology saved successfully.');

        return redirect(route('profileTechnologies.index'));
    }

    /**
     * Display the specified ProfileTechnology.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $profileTechnology = $this->profileTechnologyRepository->find($id);

        if (empty($profileTechnology)) {
            Flash::error('Profile Technology not found');

            return redirect(route('profileTechnologies.index'));
        }

        return view('profile_technologies.show')->with('profileTechnology', $profileTechnology);
    }

    /**
     * Show the form for editing the specified ProfileTechnology.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $profileTechnology = $this->profileTechnologyRepository->find($id);

        if (empty($profileTechnology)) {
            Flash::error('Profile Technology not found');

            return redirect(route('profileTechnologies.index'));
        }

        return view('profile_technologies.edit')->with('profileTechnology', $profileTechnology);
    }

    /**
     * Update the specified ProfileTechnology in storage.
     *
     * @param int $id
     * @param UpdateProfileTechnologyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfileTechnologyRequest $request)
    {
        $profileTechnology = $this->profileTechnologyRepository->find($id);

        if (empty($profileTechnology)) {
            Flash::error('Profile Technology not found');

            return redirect(route('profileTechnologies.index'));
        }

        $profileTechnology = $this->profileTechnologyRepository->update($request->all(), $id);

        Flash::success('Profile Technology updated successfully.');

        return redirect(route('profileTechnologies.index'));
    }

    /**
     * Remove the specified ProfileTechnology from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $profileTechnology = $this->profileTechnologyRepository->find($id);

        if (empty($profileTechnology)) {
            Flash::error('Profile Technology not found');

            return redirect(route('profileTechnologies.index'));
        }

        $this->profileTechnologyRepository->delete($id);

        Flash::success('Profile Technology deleted successfully.');

        return redirect(route('profileTechnologies.index'));
    }
}
