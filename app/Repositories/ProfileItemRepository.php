<?php

namespace App\Repositories;

use App\Models\ProfileItem;
use App\Repositories\BaseRepository;

/**
 * Class ProfileItemRepository
 * @package App\Repositories
 * @version February 13, 2021, 1:15 am UTC
*/

class ProfileItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'profile_id',
        'html_icon',
        'html_image',
        'label',
        'value',
        'link'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProfileItem::class;
    }
}
