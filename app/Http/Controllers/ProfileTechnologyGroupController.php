<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfileTechnologyGroupRequest;
use App\Http\Requests\UpdateProfileTechnologyGroupRequest;
use App\Repositories\ProfileTechnologyGroupRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProfileTechnologyGroupController extends AppBaseController
{
    /** @var  ProfileTechnologyGroupRepository */
    private $profileTechnologyGroupRepository;

    public function __construct(ProfileTechnologyGroupRepository $profileTechnologyGroupRepo)
    {
        $this->profileTechnologyGroupRepository = $profileTechnologyGroupRepo;
    }

    /**
     * Display a listing of the ProfileTechnologyGroup.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $profileTechnologyGroups = $this->profileTechnologyGroupRepository->all();

        return view('profile_technology_groups.index')
            ->with('profileTechnologyGroups', $profileTechnologyGroups);
    }

    /**
     * Show the form for creating a new ProfileTechnologyGroup.
     *
     * @return Response
     */
    public function create()
    {
        return view('profile_technology_groups.create');
    }

    /**
     * Store a newly created ProfileTechnologyGroup in storage.
     *
     * @param CreateProfileTechnologyGroupRequest $request
     *
     * @return Response
     */
    public function store(CreateProfileTechnologyGroupRequest $request)
    {
        $input = $request->all();

        $profileTechnologyGroup = $this->profileTechnologyGroupRepository->create($input);

        Flash::success('Profile Technology Group saved successfully.');

        return redirect(route('profileTechnologyGroups.index'));
    }

    /**
     * Display the specified ProfileTechnologyGroup.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $profileTechnologyGroup = $this->profileTechnologyGroupRepository->find($id);

        if (empty($profileTechnologyGroup)) {
            Flash::error('Profile Technology Group not found');

            return redirect(route('profileTechnologyGroups.index'));
        }

        return view('profile_technology_groups.show')->with('profileTechnologyGroup', $profileTechnologyGroup);
    }

    /**
     * Show the form for editing the specified ProfileTechnologyGroup.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $profileTechnologyGroup = $this->profileTechnologyGroupRepository->find($id);

        if (empty($profileTechnologyGroup)) {
            Flash::error('Profile Technology Group not found');

            return redirect(route('profileTechnologyGroups.index'));
        }

        return view('profile_technology_groups.edit')->with('profileTechnologyGroup', $profileTechnologyGroup);
    }

    /**
     * Update the specified ProfileTechnologyGroup in storage.
     *
     * @param int $id
     * @param UpdateProfileTechnologyGroupRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfileTechnologyGroupRequest $request)
    {
        $profileTechnologyGroup = $this->profileTechnologyGroupRepository->find($id);

        if (empty($profileTechnologyGroup)) {
            Flash::error('Profile Technology Group not found');

            return redirect(route('profileTechnologyGroups.index'));
        }

        $profileTechnologyGroup = $this->profileTechnologyGroupRepository->update($request->all(), $id);

        Flash::success('Profile Technology Group updated successfully.');

        return redirect(route('profileTechnologyGroups.index'));
    }

    /**
     * Remove the specified ProfileTechnologyGroup from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $profileTechnologyGroup = $this->profileTechnologyGroupRepository->find($id);

        if (empty($profileTechnologyGroup)) {
            Flash::error('Profile Technology Group not found');

            return redirect(route('profileTechnologyGroups.index'));
        }

        $this->profileTechnologyGroupRepository->delete($id);

        Flash::success('Profile Technology Group deleted successfully.');

        return redirect(route('profileTechnologyGroups.index'));
    }
}
