<?php

namespace App\Repositories;

use App\Models\ProfileSkill;
use App\Repositories\BaseRepository;

/**
 * Class ProfileSkillRepository
 * @package App\Repositories
 * @version February 13, 2021, 1:17 am UTC
*/

class ProfileSkillRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'html_icon',
        'image_icon',
        'label',
        'value'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProfileSkill::class;
    }
}
