<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QuoteRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'email' => $this->email,
            'company_name' => $this->company_name,
            'company_description' => $this->company_description,
            'company_link' => $this->company_link,
            'reason_for_rebuild' => $this->reason_for_rebuild,
            'competitor_links' => $this->competitor_links,
            'logo_url' => $this->logo_url,
            'app_size' => $this->app_size,
            'ui_level' => $this->ui_level,
            'user_account_email' => $this->user_account_email,
            'user_account_phone' => $this->user_account_phone,
            'user_account_facebook' => $this->user_account_facebook,
            'user_account_twitter' => $this->user_account_twitter,
            'user_account_google' => $this->user_account_google,
            'user_account_linkedin' => $this->user_account_linkedin,
            'user_account_github' => $this->user_account_github,
            'user_account_email_invitation' => $this->user_account_email_invitation,
            'user_account_multitenant' => $this->user_account_multitenant,
            'user_account_subdomain' => $this->user_account_subdomain,
            'user_account_custom_domain' => $this->user_account_custom_domain,
            'ugc_dashboard' => $this->ugc_dashboard,
            'ugc_activity_feed' => $this->ugc_activity_feed,
            'ugc_file_upload' => $this->ugc_file_upload,
            'ugc_user_profile' => $this->ugc_user_profile,
            'ugc_transactional_emails' => $this->ugc_transactional_emails,
            'ugc_tags' => $this->ugc_tags,
            'ugc_ratings_or_ratings' => $this->ugc_ratings_or_ratings,
            'ugc_media_processing' => $this->ugc_media_processing,
            'ugc_free_text_searching' => $this->ugc_free_text_searching,
            'date_calendaring' => $this->date_calendaring,
            'map_geolocation' => $this->map_geolocation,
            'custom_map' => $this->custom_map,
            'bookings' => $this->bookings,
            'in_app_messaging' => $this->in_app_messaging,
            'forums_and_commenting' => $this->forums_and_commenting,
            'social_sharing' => $this->social_sharing,
            'subscription_plans' => $this->subscription_plans,
            'payment_processing' => $this->payment_processing,
            'shoping_cart' => $this->shoping_cart,
            'user_marketplace' => $this->user_marketplace,
            'product_management' => $this->product_management,
            'cms' => $this->cms,
            'user_admin_pages' => $this->user_admin_pages,
            'content_moderation' => $this->content_moderation,
            'intercom_or_chatbot' => $this->intercom_or_chatbot,
            'usage_analytics' => $this->usage_analytics,
            'crash_reporting' => $this->crash_reporting,
            'performance_monitoring' => $this->performance_monitoring,
            'multilingual_support' => $this->multilingual_support,
            'third_party_apis' => $this->third_party_apis,
            'api' => $this->api,
            'sms_messaging' => $this->sms_messaging,
            'blog' => $this->blog,
            'user_testimonials' => $this->user_testimonials,
            'ssl_certificate' => $this->ssl_certificate,
            'dos_protection' => $this->dos_protection,
            'two_factor_auth' => $this->two_factor_auth,
            'app_icon_design' => $this->app_icon_design,
            'cloud_syncing' => $this->cloud_syncing,
            'barcode_qrcode' => $this->barcode_qrcode,
            'notifications' => $this->notifications,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
