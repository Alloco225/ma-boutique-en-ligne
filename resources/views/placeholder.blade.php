@extends('layouts.app')

@section('styles')
<style>
    html, body{
        height: 100vh;
    }


    @media screen and (min-width:400px){
        .page{
            animation: passby 15s linear infinite alternate;
        }
    }
    @keyframes passby {
        from {
            background-position: right;
        }
        to {
            background-position: left;
        }
    }
</style>
@endsection

@section('meta')
    <meta charset='UTF-8'>
    <meta name='keywords' content='cocovico, marché, marché cocovico, brader à abidjan'>
    <meta name='description' content="Achetez, vendez, bradez, négociez sur le marché de Cocovico">
    <meta name='subject' content="Marché de cocovico">
    <meta name='copyright' content="Cocovico">
    <meta name='language' content="FF">
    <meta name='robots' content='index,follow'>
    <meta name='revised' content='Tue, Aug 31st, 2021, 7:44 pm'>
    <meta name='abstract' content=''>
    <meta name='topic' content=''>
    <meta name='summary' content=''>
    <meta name='Classification' content='Marketplace'>
    <meta name='author' content='amane.dev, hello@amane.dev'>
    <meta name='designer' content=''>
    <meta name='reply-to' content='hello@amane.dev'>
    <meta name='owner' content=''>
    <meta name='url' content='https://www.cocovico.com'>
    <meta name='identifier-URL' content='https://www.cocovico.com'>
    <meta name='directory' content='submission'>
    <meta name='pagename' content="Votre marché cocovico bientôt..">
    <meta name='category' content=''>
    <meta name='coverage' content='Worldwide'>
    <meta name='distribution' content='Global'>
    <meta name='rating' content='General'>
    <meta name='revisit-after' content='7 days'>
    <meta name='subtitle' content='Marché de cocovico'>
    <meta name='target' content='all'>
    <meta name='HandheldFriendly' content='True'>
    <meta name='MobileOptimized' content='320'>
    <meta name='date' content='Aug. 31, 2021'>
    {{-- <meta name='search_date' content='2010-09-27'> --}}
    {{-- <meta name='DC.title' content='Unstoppable Robot Ninja'> --}}
    {{-- <meta name='ResourceLoaderDynamicStyles' content=''> --}}
    {{-- <meta name='medium' content='blog'> --}}
    {{-- <meta name='syndication-source' content='https://mashable.com/2008/12/24/free-brand-monitoring-tools/'> --}}
    {{-- <meta name='original-source' content='https://mashable.com/2008/12/24/free-brand-monitoring-tools/'> --}}
    <meta name='verify-v1' content='dV1r/ZJJdDEI++fKJ6iDEl6o+TMNtSu0kv18ONeqM0I='>
    {{-- <meta name='y_key' content='1e39c508e0d87750'> --}}
    {{-- <meta name='pageKey' content='guest-home'> --}}
    {{-- <meta itemprop='name' content='jQTouch'> --}}
    {{-- <meta http-equiv='Expires' content='0'> --}}
    {{-- <meta http-equiv='Pragma' content='no-cache'>
    <meta http-equiv='Cache-Control' content='no-cache'>
    <meta http-equiv='imagetoolbar' content='no'>
    <meta http-equiv='x-dns-prefetch-control' content='off'> --}}


    <meta name="og:title" content="Cocovico"/>
    <meta name="og:type" content="Site web"/>
    <meta name="og:url" content="{{env('APP_URL')}}">
    <meta name="og:image" content="{{asset(env('APP_ICON_URL'))}}"/>
    <meta name="og:site_name" content="Cocovico"/>
    <meta name="og:description" content="Achetez, vendez, bradez"/>
@endsection
@section('content')

<div  class="page h-full w-full bg-yellow-400 flex items-center justify-center" style="background-image: url('/bg/clouds.svg')">
    {{-- <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path></svg> --}}
    <a href="/welcome">
        <img src="{{env('APP_ICON_URL')}}" class="w-32 h-32 object-contain" alt="">
    </a>
</div>

@endsection
