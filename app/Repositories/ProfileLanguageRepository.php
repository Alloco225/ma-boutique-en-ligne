<?php

namespace App\Repositories;

use App\Models\ProfileLanguage;
use App\Repositories\BaseRepository;

/**
 * Class ProfileLanguageRepository
 * @package App\Repositories
 * @version February 13, 2021, 1:20 am UTC
*/

class ProfileLanguageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'profile_id',
        'html_icon',
        'label',
        'progress',
        'value'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProfileLanguage::class;
    }
}
