<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ClientQuoteRequestPage extends Component
{

    public $data = [];

    protected $rules = [
        'data.app_size' => 'required',
    ];

    public function render()
    {
        return view('livewire.client-quote-request-page');
    }
}
