<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TechnologiesSection extends Component
{
    public $techs;

    function mount($techs){
        $this->techs = $techs;
    }

    public function render()
    {
        return view('livewire.technologies-section');
    }
}
