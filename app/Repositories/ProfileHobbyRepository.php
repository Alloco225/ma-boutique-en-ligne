<?php

namespace App\Repositories;

use App\Models\ProfileHobby;
use App\Repositories\BaseRepository;

/**
 * Class ProfileHobbyRepository
 * @package App\Repositories
 * @version February 13, 2021, 1:24 am UTC
*/

class ProfileHobbyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'profile_id',
        'html_icon',
        'html_image',
        'label'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProfileHobby::class;
    }
}
