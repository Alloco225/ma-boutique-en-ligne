<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;



/**
 * Class Commande
 * @package App\Models
 * @version October 11, 2021, 4:22 pm UTC
 *
 * @property number $total
 * @property unsignedBigInteger $id_utilisateur
 */
class Commande extends Model
{


    public $table = 'commandes';




    public $fillable = [
        'total',
        'id_utilisateur'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'total' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    // relationships
    function utilisateur(){
        return $this->belongsTo(User::class);
    }
    function commandes_produit(){
        return $this->hasMany(CommandeProduit::class);
    }
}
