<?php

namespace App\Http\Livewire;

use App\Models\Profile;
use Livewire\Component;

class MyResume extends Component
{
    public $profile;
    
    function mount(){
        $this->profile = Profile::get();
    }

    public function render()
    {
        // $this->profile = Profile::get();
        return view('livewire.my-resume');
    }
}
