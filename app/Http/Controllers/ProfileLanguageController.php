<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfileLanguageRequest;
use App\Http\Requests\UpdateProfileLanguageRequest;
use App\Repositories\ProfileLanguageRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProfileLanguageController extends AppBaseController
{
    /** @var  ProfileLanguageRepository */
    private $profileLanguageRepository;

    public function __construct(ProfileLanguageRepository $profileLanguageRepo)
    {
        $this->profileLanguageRepository = $profileLanguageRepo;
    }

    /**
     * Display a listing of the ProfileLanguage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $profileLanguages = $this->profileLanguageRepository->all();

        return view('profile_languages.index')
            ->with('profileLanguages', $profileLanguages);
    }

    /**
     * Show the form for creating a new ProfileLanguage.
     *
     * @return Response
     */
    public function create()
    {
        return view('profile_languages.create');
    }

    /**
     * Store a newly created ProfileLanguage in storage.
     *
     * @param CreateProfileLanguageRequest $request
     *
     * @return Response
     */
    public function store(CreateProfileLanguageRequest $request)
    {
        $input = $request->all();

        $profileLanguage = $this->profileLanguageRepository->create($input);

        Flash::success('Profile Language saved successfully.');

        return redirect(route('profileLanguages.index'));
    }

    /**
     * Display the specified ProfileLanguage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $profileLanguage = $this->profileLanguageRepository->find($id);

        if (empty($profileLanguage)) {
            Flash::error('Profile Language not found');

            return redirect(route('profileLanguages.index'));
        }

        return view('profile_languages.show')->with('profileLanguage', $profileLanguage);
    }

    /**
     * Show the form for editing the specified ProfileLanguage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $profileLanguage = $this->profileLanguageRepository->find($id);

        if (empty($profileLanguage)) {
            Flash::error('Profile Language not found');

            return redirect(route('profileLanguages.index'));
        }

        return view('profile_languages.edit')->with('profileLanguage', $profileLanguage);
    }

    /**
     * Update the specified ProfileLanguage in storage.
     *
     * @param int $id
     * @param UpdateProfileLanguageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfileLanguageRequest $request)
    {
        $profileLanguage = $this->profileLanguageRepository->find($id);

        if (empty($profileLanguage)) {
            Flash::error('Profile Language not found');

            return redirect(route('profileLanguages.index'));
        }

        $profileLanguage = $this->profileLanguageRepository->update($request->all(), $id);

        Flash::success('Profile Language updated successfully.');

        return redirect(route('profileLanguages.index'));
    }

    /**
     * Remove the specified ProfileLanguage from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $profileLanguage = $this->profileLanguageRepository->find($id);

        if (empty($profileLanguage)) {
            Flash::error('Profile Language not found');

            return redirect(route('profileLanguages.index'));
        }

        $this->profileLanguageRepository->delete($id);

        Flash::success('Profile Language deleted successfully.');

        return redirect(route('profileLanguages.index'));
    }
}
