<?php

namespace App\Http\Livewire;

use Livewire\Component;

class WelcomeFancy extends Component
{
    public function render()
    {
        return view('livewire.welcome-fancy');
    }
}
