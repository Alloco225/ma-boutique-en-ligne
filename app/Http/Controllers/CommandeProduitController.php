<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCommandeProduitRequest;
use App\Http\Requests\UpdateCommandeProduitRequest;
use App\Repositories\CommandeProduitRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CommandeProduitController extends AppBaseController
{
    /** @var  CommandeProduitRepository */
    private $commandeProduitRepository;

    public function __construct(CommandeProduitRepository $commandeProduitRepo)
    {
        $this->commandeProduitRepository = $commandeProduitRepo;
    }

    /**
     * Display a listing of the CommandeProduit.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $commandeProduits = $this->commandeProduitRepository->all();

        return view('commande_produits.index')
            ->with('commandeProduits', $commandeProduits);
    }

    /**
     * Show the form for creating a new CommandeProduit.
     *
     * @return Response
     */
    public function create()
    {
        return view('commande_produits.create');
    }

    /**
     * Store a newly created CommandeProduit in storage.
     *
     * @param CreateCommandeProduitRequest $request
     *
     * @return Response
     */
    public function store(CreateCommandeProduitRequest $request)
    {
        $input = $request->all();

        $commandeProduit = $this->commandeProduitRepository->create($input);

        Flash::success('Commande Produit saved successfully.');

        return redirect(route('commandeProduits.index'));
    }

    /**
     * Display the specified CommandeProduit.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $commandeProduit = $this->commandeProduitRepository->find($id);

        if (empty($commandeProduit)) {
            Flash::error('Commande Produit not found');

            return redirect(route('commandeProduits.index'));
        }

        return view('commande_produits.show')->with('commandeProduit', $commandeProduit);
    }

    /**
     * Show the form for editing the specified CommandeProduit.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $commandeProduit = $this->commandeProduitRepository->find($id);

        if (empty($commandeProduit)) {
            Flash::error('Commande Produit not found');

            return redirect(route('commandeProduits.index'));
        }

        return view('commande_produits.edit')->with('commandeProduit', $commandeProduit);
    }

    /**
     * Update the specified CommandeProduit in storage.
     *
     * @param int $id
     * @param UpdateCommandeProduitRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommandeProduitRequest $request)
    {
        $commandeProduit = $this->commandeProduitRepository->find($id);

        if (empty($commandeProduit)) {
            Flash::error('Commande Produit not found');

            return redirect(route('commandeProduits.index'));
        }

        $commandeProduit = $this->commandeProduitRepository->update($request->all(), $id);

        Flash::success('Commande Produit updated successfully.');

        return redirect(route('commandeProduits.index'));
    }

    /**
     * Remove the specified CommandeProduit from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $commandeProduit = $this->commandeProduitRepository->find($id);

        if (empty($commandeProduit)) {
            Flash::error('Commande Produit not found');

            return redirect(route('commandeProduits.index'));
        }

        $this->commandeProduitRepository->delete($id);

        Flash::success('Commande Produit deleted successfully.');

        return redirect(route('commandeProduits.index'));
    }
}
