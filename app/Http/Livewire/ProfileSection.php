<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProfileSection extends Component
{
    public $profile;
    public $profile_items;

    function mount($profile){
        $this->profile = $profile;
        $this->profile_items = $profile->items;
    }
    public function render()
    {
        return view('livewire.profile-section');
    }
}
