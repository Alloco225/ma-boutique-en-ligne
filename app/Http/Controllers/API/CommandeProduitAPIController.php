<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCommandeProduitAPIRequest;
use App\Http\Requests\API\UpdateCommandeProduitAPIRequest;
use App\Models\CommandeProduit;
use App\Repositories\CommandeProduitRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\CommandeProduitResource;
use Response;

/**
 * Class CommandeProduitController
 * @package App\Http\Controllers\API
 */

class CommandeProduitAPIController extends AppBaseController
{
    /** @var  CommandeProduitRepository */
    private $commandeProduitRepository;

    public function __construct(CommandeProduitRepository $commandeProduitRepo)
    {
        $this->commandeProduitRepository = $commandeProduitRepo;
    }

    /**
     * Display a listing of the CommandeProduit.
     * GET|HEAD /commandeProduits
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $commandeProduits = $this->commandeProduitRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(CommandeProduitResource::collection($commandeProduits), 'Commande Produits retrieved successfully');
    }

    /**
     * Store a newly created CommandeProduit in storage.
     * POST /commandeProduits
     *
     * @param CreateCommandeProduitAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCommandeProduitAPIRequest $request)
    {
        $input = $request->all();

        $commandeProduit = $this->commandeProduitRepository->create($input);

        return $this->sendResponse(new CommandeProduitResource($commandeProduit), 'Commande Produit saved successfully');
    }

    /**
     * Display the specified CommandeProduit.
     * GET|HEAD /commandeProduits/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CommandeProduit $commandeProduit */
        $commandeProduit = $this->commandeProduitRepository->find($id);

        if (empty($commandeProduit)) {
            return $this->sendError('Commande Produit not found');
        }

        return $this->sendResponse(new CommandeProduitResource($commandeProduit), 'Commande Produit retrieved successfully');
    }

    /**
     * Update the specified CommandeProduit in storage.
     * PUT/PATCH /commandeProduits/{id}
     *
     * @param int $id
     * @param UpdateCommandeProduitAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommandeProduitAPIRequest $request)
    {
        $input = $request->all();

        /** @var CommandeProduit $commandeProduit */
        $commandeProduit = $this->commandeProduitRepository->find($id);

        if (empty($commandeProduit)) {
            return $this->sendError('Commande Produit not found');
        }

        $commandeProduit = $this->commandeProduitRepository->update($input, $id);

        return $this->sendResponse(new CommandeProduitResource($commandeProduit), 'CommandeProduit updated successfully');
    }

    /**
     * Remove the specified CommandeProduit from storage.
     * DELETE /commandeProduits/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CommandeProduit $commandeProduit */
        $commandeProduit = $this->commandeProduitRepository->find($id);

        if (empty($commandeProduit)) {
            return $this->sendError('Commande Produit not found');
        }

        $commandeProduit->delete();

        return $this->sendSuccess('Commande Produit deleted successfully');
    }
}
