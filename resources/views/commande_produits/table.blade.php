<div class="table-responsive">
    <table class="table" id="commandeProduits-table">
        <thead>
            <tr>
                <th>Id Commande</th>
        <th>Id Produit</th>
        <th>Quantite</th>
        <th>Montant</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($commandeProduits as $commandeProduit)
            <tr>
                <td>{{ $commandeProduit->id_commande }}</td>
            <td>{{ $commandeProduit->id_produit }}</td>
            <td>{{ $commandeProduit->quantite }}</td>
            <td>{{ $commandeProduit->montant }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['commandeProduits.destroy', $commandeProduit->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('commandeProduits.show', [$commandeProduit->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('commandeProduits.edit', [$commandeProduit->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
