<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQuoteRequestRequest;
use App\Http\Requests\UpdateQuoteRequestRequest;
use App\Repositories\QuoteRequestRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class QuoteRequestController extends AppBaseController
{
    /** @var  QuoteRequestRepository */
    private $quoteRequestRepository;

    public function __construct(QuoteRequestRepository $quoteRequestRepo)
    {
        $this->quoteRequestRepository = $quoteRequestRepo;
    }

    /**
     * Display a listing of the QuoteRequest.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $quoteRequests = $this->quoteRequestRepository->all();

        return view('quote_requests.index')
            ->with('quoteRequests', $quoteRequests);
    }

    /**
     * Show the form for creating a new QuoteRequest.
     *
     * @return Response
     */
    public function create()
    {
        return view('quote_requests.create');
    }

    /**
     * Store a newly created QuoteRequest in storage.
     *
     * @param CreateQuoteRequestRequest $request
     *
     * @return Response
     */
    public function store(CreateQuoteRequestRequest $request)
    {
        $input = $request->all();

        $quoteRequest = $this->quoteRequestRepository->create($input);

        Flash::success('Quote Request saved successfully.');

        return redirect(route('quoteRequests.index'));
    }

    /**
     * Display the specified QuoteRequest.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $quoteRequest = $this->quoteRequestRepository->find($id);

        if (empty($quoteRequest)) {
            Flash::error('Quote Request not found');

            return redirect(route('quoteRequests.index'));
        }

        return view('quote_requests.show')->with('quoteRequest', $quoteRequest);
    }

    /**
     * Show the form for editing the specified QuoteRequest.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $quoteRequest = $this->quoteRequestRepository->find($id);

        if (empty($quoteRequest)) {
            Flash::error('Quote Request not found');

            return redirect(route('quoteRequests.index'));
        }

        return view('quote_requests.edit')->with('quoteRequest', $quoteRequest);
    }

    /**
     * Update the specified QuoteRequest in storage.
     *
     * @param int $id
     * @param UpdateQuoteRequestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQuoteRequestRequest $request)
    {
        $quoteRequest = $this->quoteRequestRepository->find($id);

        if (empty($quoteRequest)) {
            Flash::error('Quote Request not found');

            return redirect(route('quoteRequests.index'));
        }

        $quoteRequest = $this->quoteRequestRepository->update($request->all(), $id);

        Flash::success('Quote Request updated successfully.');

        return redirect(route('quoteRequests.index'));
    }

    /**
     * Remove the specified QuoteRequest from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $quoteRequest = $this->quoteRequestRepository->find($id);

        if (empty($quoteRequest)) {
            Flash::error('Quote Request not found');

            return redirect(route('quoteRequests.index'));
        }

        $this->quoteRequestRepository->delete($id);

        Flash::success('Quote Request deleted successfully.');

        return redirect(route('quoteRequests.index'));
    }
}
